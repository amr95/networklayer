//
//  APIHelper.swift
//  NetworkLayer
//
//  Created by apple on 9/8/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation
import Alamofire

class APIHelper {

  class func fetchGenericsData<T: Decodable>(stringURL:String , parameters: [String:Any]? , completionHandler: @escaping (T) -> ()) {
    print("URL = \(stringURL)")
    Alamofire.request(stringURL, method: .get, parameters: parameters, encoding: URLEncoding.queryString, headers: nil)
      .responseJSON { (resposneData) in
         let data = resposneData
        print(data)
        do {
          let obj = try JSONDecoder().decode(T.self, from: data.data!)
          print("obj = \(obj)")
          completionHandler(obj)
        } catch {
          print("ERROR")
        }
        
    }
  }
  
}
