//
//  News.swift
//  NetworkLayer
//
//  Created by apple on 9/8/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation


// MARK: - News
struct News: Decodable {
  let success: Bool?
  let code: Int?
  let message: String?
  let data: [NewsData]?
}

// MARK: - Datum
struct NewsData: Decodable {
  let postExcerpt: String?
  let id: Int?
  let link: String?
  let title: String?
  let postImage: String?
  let dateCreated, dateUpdated: Int?
  let featured: Bool?
  
  enum CodingKeys: String, CodingKey {
    case postExcerpt = "post_excerpt"
    case id, link, title
    case postImage = "post_image"
    case dateCreated = "date_created"
    case dateUpdated = "date_updated"
    case featured
  }
  
}
