//
//  APIResponse.swift
//  NetworkLayer
//
//  Created by apple on 9/8/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation

struct APIResponse: Decodable {
  let success: Bool?
  let code: Int?
  let message: String?
}
