//
//  Blogs.swift
//  NetworkLayer
//
//  Created by apple on 9/8/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation

// MARK: - Blog
struct Blog: Decodable {
  let success: Bool?
  let code: Int?
  let message: String?
  let data: [BlogsData]?
}

// MARK: - Datum
struct BlogsData: Decodable {
  let postExcerpt: String?
  let id: Int?
  let link: String?
  let dateCreated, dateUpdated: Int?
  let postCategory: PostCategory?
  let postImage: String?
  let title: String?
  let featured: Bool?
  
  enum CodingKeys: String, CodingKey {
    case postExcerpt = "post_excerpt"
    case id, link
    case dateCreated = "date_created"
    case dateUpdated = "date_updated"
    case postCategory = "post_category"
    case postImage = "post_image"
    case title, featured
  }
}

// MARK: - PostCategory
struct PostCategory: Decodable {
  let id: Int?
  let name: String?
}
