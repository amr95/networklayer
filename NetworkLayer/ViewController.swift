//
//  ViewController.swift
//  NetworkLayer
//
//  Created by apple on 9/8/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  @IBOutlet weak var tittleLabel: UILabel!
  @IBOutlet weak var idLabel: UILabel!
  
  @IBOutlet weak var blogIDLabel: UILabel!
  @IBOutlet weak var blogTittleLabel: UILabel!
  
  fileprivate func fetchNews() {
    // Do any additional setup after loading the view, typically from a nib.
    let url = "http://magrabidev.code95.info/api/wp/v2/news"
    let params = ["per_page": 5, "page": 1]
    APIHelper.fetchGenericsData(stringURL: url, parameters: params) { (news: News) in
      let newsData = news.data as! [NewsData]
      self.idLabel.text = String(newsData[0].id!)
      self.tittleLabel.text = newsData[0].title
    }
  }
  
  fileprivate func fetchBlogs() {
    // Do any additional setup after loading the view, typically from a nib.
    let url = "http://magrabidev.code95.info/api/wp/v2/blog"
    let params = ["per_page": 5, "page": 1]
    APIHelper.fetchGenericsData(stringURL: url, parameters: params) { (blogs: Blog) in
      let blogsData = blogs.data as! [BlogsData]
      self.blogIDLabel.text = String(blogsData[0].id!)
      self.blogTittleLabel.text = blogsData[0].title
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    fetchNews()
    fetchBlogs()
  }


}

