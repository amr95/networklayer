//
//  Constant.swift
//  NetworkLayer
//
//  Created by apple on 9/8/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation

public enum NetworkReply {
  case success
  case failure
}
